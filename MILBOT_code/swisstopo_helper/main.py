import imageio
from helpers import *

def glacier_trient():
    print("# Capturing images of 'Glacier du Trient'")

    name = "trient"
    gif_images = []

    for year in range(1863, 2021, 1):
        print("## Currently at year {}".format(year))
        download_pictures(name, "https://map.geo.admin.ch/?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time={}&layers_timestamp={}1231&E=2565948.72&N=1096086.68&zoom=5.863333333333329".format(year, year), year)

        left = 830
        top = 400
        right = 1750
        bottom = 1800

        resize_image(name, year, left, top, right, bottom)

        gif_images.append(imageio.imread("./pictures/trient/{}_cropped.png".format(year)))

    imageio.mimsave("./pictures/trient/all_cropped.gif", gif_images)

def glacier_aletsch():
    print("# Capturing images of 'Glacier d'Aletsch'")

    name = "aletsch"
    gif_images = []

    for year in range(1915, 2021, 1):
        print("## Currently at year {}".format(year))
        download_pictures(name, "https://map.geo.admin.ch/?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time={}&layers_timestamp={}1231&E=2645193.20&N=1141820.68&zoom=5.299999999999983".format(year, year), year)

        left = 650
        top = 390
        right = 1775
        bottom = 1830

        resize_image(name, year, left, top, right, bottom)

        gif_images.append(imageio.imread("./pictures/aletsch/{}_cropped.png".format(year)))

    imageio.mimsave("./gif/aletsch/all_cropped.gif", gif_images)

def glacier_pizol():
    print("# Capturing images of 'Pizolgletscher'")

    name = "pizol"
    gif_images = []

    for year in range(1890, 2021, 1):
        print("## Currently at year {}".format(year))
        download_pictures(name, "https://map.geo.admin.ch/?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time={}&layers_timestamp={}1231&E=2748139.21&N=1203333.98&zoom=7.778594761554025".format(year, year), year)

        left = 650
        top = 390
        right = 1775
        bottom = 1830

        resize_image(name, year, left, top, right, bottom)

        gif_images.append(imageio.imread("./pictures/pizol/{}_cropped.png".format(year)))

    imageio.mimsave("./gif/pizol/all_cropped.gif", gif_images)

if __name__ == '__main__':
    glacier_pizol()