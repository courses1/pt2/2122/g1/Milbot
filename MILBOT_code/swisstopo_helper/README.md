# swisstopo_helper

The functions in `main.py` allow to create animated GIFs of the evolution of the SwissTopo maps over time.

## iFrame

### Pizol

- <iframe src='https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=1920&layers_timestamp=19201231&E=2748387.10&N=1202914.19&zoom=7.518594761554023' width='400' height='300' frameborder='0' style='border:0'></iframe>
- <iframe src='https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=1961&layers_timestamp=19611231&E=2748387.10&N=1202914.19&zoom=7.518594761554023' width='400' height='300' frameborder='0' style='border:0'></iframe>
- <iframe src='https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=1999&layers_timestamp=19991231&E=2748387.10&N=1202914.19&zoom=7.518594761554023' width='400' height='300' frameborder='0' style='border:0'></iframe>
- <iframe src='https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=2003&layers_timestamp=20031231&E=2748387.10&N=1202914.19&zoom=7.518594761554023' width='400' height='300' frameborder='0' style='border:0'></iframe>
- <iframe src='https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=2018&layers_timestamp=20181231&E=2748387.10&N=1202914.19&zoom=7.518594761554023' width='400' height='300' frameborder='0' style='border:0'></iframe>

### Gornergrat

- <iframe src='https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=1920&layers_timestamp=19201231&E=2627496.52&N=1092881.29&zoom=4.320634829847408' width='400' height='300' frameborder='0' style='border:0'></iframe>
- <iframe src='https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=1930&layers_timestamp=19301231&E=2627496.52&N=1092881.29&zoom=4.320634829847408' width='400' height='300' frameborder='0' style='border:0'></iframe>
- <iframe src='https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=1968&layers_timestamp=19681231&E=2627496.52&N=1092881.29&zoom=4.320634829847408' width='400' height='300' frameborder='0' style='border:0'></iframe>
- <iframe src='https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=1990&layers_timestamp=19901231&E=2627496.52&N=1092881.29&zoom=4.320634829847408' width='400' height='300' frameborder='0' style='border:0'></iframe>
- <iframe src='https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=2018&layers_timestamp=20181231&E=2627496.52&N=1092881.29&zoom=4.320634829847408' width='400' height='300' frameborder='0' style='border:0'></iframe>